---
title: bilibili官方客户端移植版
date: 2022-08-10T11:29:06+08:00
author: kero990
# 这里写你的名字
avatar: https://gitee.com/kero990/web/raw/master/132.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://gitee.com/gfdgd-xi/
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - aarch64
tags:
  - 视频播放
cover: https://gitee.com/kero990/web/raw/master/03183575566c62f000001cc296b3055.jpg
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： bilibili官方客户端移植版

应用分类： 视频播放

适配架构： aarch64

反馈渠道： https://bbs.chinauos.com/zh/post/12383 



<!--more-->


官方网址：

https://www.biliblili.com/  


下载链接： 
 
蓝奏云（arm64）：

https://kero990.lanzoum.com/inbAp05n1dud

程序介绍：
  
&nbsp;&nbsp;bilibili官方客户端移植版,由官方windows版客户端移植而来 
应用截图：  
![](https://img.lovestu.com/uploads/2022/06/20220627160719.webp)  
