---
title: Drawio开源跨平台绘图软件
date: 2022-08-10T11:29:06+08:00
author: kero990
# 这里写你的名字
avatar: https://gitee.com/kero990/web/raw/master/132.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://gitee.com/gfdgd-xi/
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - aarch64
tags:
  - 图形图像
#cover: https://z3.ax1x.com/2021/07/09/RvZcZt.png
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： Drawio开源跨平台绘图软件

应用分类： 图形图像

适配架构： aarch64

反馈渠道： https://bbs.chinauos.com/zh/post/12383 



<!--more-->


官方网址：

https://www.diagrams.net/
https://github.com/jgraph/drawio-desktop


下载链接： 
 
蓝奏云（arm64）：

https://kero990.lanzoum.com/iH8QD06k2oib

程序介绍：
  
&nbsp;&nbsp;draw.io是一款非常不错的流程图绘制软件，支持各种各样的流程图绘制，基本上能代替Visio和亿图图示等软件了，并且软件开源跨平台，任何用户都能免费使用。

本软件是一款非常不错的替代软件。支持中文等多语言，值得使用。
应用截图：  
![](https://img.lovestu.com/uploads/2020/07/20200731155620.jpg)  
![](https://img.lovestu.com/uploads/2020/07/20200731160106.jpg)  