---
title: koodo Reader 阅读器
date: 2022-08-10T11:29:06+08:00
author: kero990
# 这里写你的名字
avatar: https://gitee.com/kero990/web/raw/master/132.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://gitee.com/gfdgd-xi/
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - aarch64
tags:
  - 阅读翻译
cover: https://koodo.960960.xyz/images/label.png
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： koodo Reader 电子书阅读器

应用分类： 阅读翻译

适配架构： aarch64

反馈渠道： https://bbs.chinauos.com/zh/post/12383 



<!--more-->


官方网址：

https://gitee.com/troyeguo/koodo-reader 
https://koodo.960960.xyz/ 


下载链接： 
 
蓝奏云（arm64）：

https://kero990.lanzoum.com/idXJZ05n1eqf

程序介绍：
  
&nbsp;&nbsp;Koodo Reader 是一个跨平台的电子书阅读器。平台支持Windows，macOS，Linux 和网页版，格式支持 epub, pdf, mobi, azw3, txt, djvu, markdown, fb2, cbz, cbt, cbr, rtf 和 docx。
应用截图：  
![](https://img.lovestu.com/uploads/2021/10/20211018102347.jpg)  
