---
title: xmind思维导图
date: 2022-08-10T11:29:06+08:00
author: xmind.cn、kero990（编译）
# 这里写你的名字
avatar: https://gitee.com/kero990/web/raw/master/132.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://gitee.com/gfdgd-xi/
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - aarch64
tags:
  - 图形图像
cover: https://s3.cn-north-1.amazonaws.com.cn/assets.xmind.cn/www/assets/images/download/new/download-logo-08e086aefe.svg
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： xmind思维导图

应用分类： 图形图像

适配架构： aarch64

反馈渠道： https://bbs.chinauos.com/zh/post/12383 



<!--more-->


官方网址：

https://xmind.cn/  


下载链接： 
 
蓝奏云（arm64）：

https://kero990.lanzoum.com/iUWzU09as3lc

程序介绍：
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Xmind 是一款 全功能 的思维导图和头脑风暴软件。像大脑的瑞士军刀一般，助你理清思路，捕捉创意。  
应用截图：  
![](https://s3.cn-north-1.amazonaws.com.cn/assets.xmind.cn/www/assets/images/xmind2022-homepage-new/cn/platform-mac-8300976d4b.png)  
![](https://s3.cn-north-1.amazonaws.com.cn/assets.xmind.cn/www/assets/images/xmind2022-homepage-new/cn/match-color-theme-e24a507d54.png)  
