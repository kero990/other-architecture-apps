---
title: UEngine 运行器
date: 2022-08-09T01:29:06+08:00
author: gfdgd xi、为什么您不喜欢熊出没和阿布呢
# 这里写你的名字
avatar: https://bbs.deepin.org/assets/image/raccoon/blush.gif
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://gitee.com/gfdgd-xi/
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - aarch64
tags:
  - 系统工具
cover: https://deepin-community-store.gitee.io/other-architecture-apps/img/default1.jpg
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： UEngine 运行器

应用分类： 系统工具

适配架构： aarch64

反馈渠道： https://bbs.deepin.org/post/240944 



<!--more-->


官方网址：

https://gitee.com/gfdgd-xi/uengine-runner

https://github.com/gfdgd-xi/uengine-runner

https://gitlink.org.cn/gfdgd_xi/uengine-runner  


下载链接： 
 
蓝奏云（全版本）：

https://gfdgdxi.lanzoui.com/b01oaxnbi

密码：2rh3 

程序介绍：
  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;新版本Deepin/UOS发布后，可以在应用商店安装部分官方已适配的安卓应用，对爱好者来说，不能自己安装APK软件包始终差点意思，本程序可以为Deepin/UOS上的UEngine安卓运行环境安装自定义APK软件包，并能发送安装的APK包启动菜单到桌面或系统菜单。  
应用截图：  
![](https://storage.deepin.org/thread/202207271700065629_image.png)  
![f4284751a7926eaf9bc8f767bec62d6.jpg](https://storage.deepin.org/thread/202208082144097465_f4284751a7926eaf9bc8f767bec62d6.jpg)  
