---
title: Icalingua++
date: 2022-08-08T19:37:44+08:00
author: shenmo
# 这里写作者名字
avatar: https://deepin-community-store.gitee.io/other-architecture-apps/img/ai-avatar.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
authorlink: https://shenmo7192.gitee.io/
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - aarch64
tags:
  - 社交沟通
cover: https://deepin-community-store.gitee.io/other-architecture-apps/img/default1.jpg
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： Icalingua++

应用分类： 社交沟通

适配架构： aarch64


<!--more-->

`Icalingua++`是一款跨平台的第三方QQ应用，在Linux上的表现远远强于`LinuxQQ`

在此分享`aarch64(arm64)`架构版本的软件包

官方网址： https://github.com/Icalingua-plus-plus/Icalingua-plus-plus/

下载地址： https://github.com/Icalingua-plus-plus/Icalingua-plus-plus/releases/

国内镜像（版本为2.6.6）：  https://shenmo.lanzoul.com/ij0Ry097pq7a

应用截图： ![screen_2](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/screen_2.png)