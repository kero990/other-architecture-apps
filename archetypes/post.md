---
title: {{ replace .TranslationBaseName "-" " " | title }}
date: {{ .Date }}
author: Author Name
# 这里写你的名字
avatar: https://deepin-community-store.gitee.io/other-architecture-apps/img/ai-avatar.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://author.site
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - 适配架构（aarch64/loongarch/mipsel等）
tags:
  - 分类信息（比如网络应用/社交沟通/音乐欣赏/视频播放/图形图像/游戏娱乐/办公学习/阅读翻译/编程开发/系统工具/主题美化/其他应用）
cover: https://deepin-community-store.gitee.io/other-architecture-apps/img/default1.jpg
# 上面写页面的封面链接。如果不写，会放置默认的封面
# nolastmod: true
draft: false

---

应用分享： 应用名

应用分类： 应同上方信息区的分类信息（tags）

适配架构： 应同上方信息区的适配架构（categories）

反馈渠道： 可为论坛帖子/贴吧帖子/邮箱地址/QQ群/etc....，以能进行讨论为佳。

<!--more-->

在这里写下正文


