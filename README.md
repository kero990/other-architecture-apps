# 非X86架构应用收集

#### 介绍
可在此分享非X86的应用

#### 分享

* fork这个仓库到你自己的名下
  
![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955653219.png)



![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955695420.png)

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955725472.png)



* clone 你的名下的仓库到你的电脑（注意不是这个仓库）

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955804565.png)

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955845173.png)



* 进入仓库目录，执行 `hugo new post/"这里写页面名称.md"` 如果提示没有hugo，请前往 spk://store/office/hugo 获取

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955902047.png)

3. 到`content/post`下找到这个md文件，打开并编写

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659955957076.png)
以下为你会看到的内容，请按要求修改

```
---
title: 这是一个例子
date: 2022-08-08T18:51:31+08:00
author: Author Name
# 这里写你的名字
avatar: /img/ai-avatar.jpeg
# 这里写你的头像的地址的链接，如果不改动，默认为Kizuna Ai的头像
#authorlink: https://author.site
# 上面写你的网址，可以写某个平台的个人中心网址（比如各种论坛，b站）。如果要显示，请把#删去
categories:
  - 适配架构（aarch64/loongarch/mipsel等）
tags:
  - 分类信息（比如网络应用/社交沟通/音乐欣赏/视频播放/图形图像/游戏娱乐/办公学习/阅读翻译/编程开发/系统工具/主题美化/其他应用）
#cover: link_here
# 上面写页面的封面链接。如果不写，会从默认的封面选一个如果要显示，请把#删去
# nolastmod: true
draft: false

---

应用分享： 应用名
应用分类： 应同上方信息区的分类信息（tags）
适配架构： 应同上方信息区的适配架构（categories）

<!--more-->

在这里写下正文




```

按照需求修改，写完后保存。推荐使用应用：小书匠 spk://store/office/com.story-writer.spark



* 确定无误后，执行`upload.sh`

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659956298247.png)
点击在终端中运行

途中需要你输入你的码云账号和密码，根据提示输入即可


* 进入码云，到你的仓库，点击`pull request`，然后创建一个pull request

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659956348987.png)

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659956421906.png)

![enter description here](https://xiaoshujiang-shenmo.oss-accelerate.aliyuncs.com/小书匠/1659956444492.png)

* 等待审核通过被合入

## 软件更新时更新帖子的方式
直接修改原帖子的页面文件即可，修改完提Pr，合并后会按照更新时间自动更新到最前